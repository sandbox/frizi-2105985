(function ($) {
  Drupal.behaviors.wwwlib = {
    attach: function (context, settings) {
    var tmpKey, tmpLat, tmpLng, mapOptions, input, map, autocomplete, place;

	jQuery.fn.extend({
		initMap: function(mapId) {

			tmpLat = $('.lat-' + mapId).val() != 0 ? $('.lat-' + mapId).val() : false;
			tmpLng = $('.lng-' + mapId).val() != 0 ? $('.lng-' + mapId).val() : false;

			// if lat and lng values available
			if (tmpLat && tmpLng) {
				mapOptions = {
						center: new google.maps.LatLng($('.lat-' + mapId).val(), $('.lng-' + mapId).val()),
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById('map_canvas-' + mapId), mapOptions);
			}
		},
		searchAddr: function(mapId) {

			input = (document.getElementById($(this).attr('id')));
			autocomplete = new google.maps.places.Autocomplete(input);

			google.maps.event.addListener(autocomplete, 'place_changed', function() {
	    		place = autocomplete.getPlace();
	    		$('.lat-' + mapId).val(place.geometry.location.lat());
	    		$('.lng-' + mapId).val(place.geometry.location.lng());
	    		$(this).initMap(mapId);
	    	});
		}
	});

	$('.autocompl').click(function(){
		$(this).searchAddr($(this).attr('id'));
	});
	
	$('.autocompl').each(function(key, value){
		$(this).initMap();
	});

    }
  };

}(jQuery));